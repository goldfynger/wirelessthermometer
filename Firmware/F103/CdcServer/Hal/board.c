#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include "stm32f1xx_hal.h"
#include "spi.h"
#include "nrf24.h"
#include "main.h"
#include "board.h"


static __align(2) uint8_t _spiTxAlignedBuffer[NRF24_MAX_DATA_LENGTH]; /* SPI buffer must be aligned to 2. */
static __align(2) uint8_t _spiRxAlignedBuffer[NRF24_MAX_DATA_LENGTH]; /* SPI buffer must be aligned to 2. */


static NRF24_ErrorTypeDef BOARD_TransmitReceiveSync(uint8_t *pTxRxData, uint16_t length, bool txFlag, bool rxFlag);


void BOARD_SetTxLedState(bool txLedState)
{
    HAL_GPIO_WritePin(LED_TX_GPIO_Port, LED_TX_Pin, txLedState ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

void BOARD_SetRxLedState(bool rxLedState)
{
    HAL_GPIO_WritePin(LED_RX_GPIO_Port, LED_RX_Pin, rxLedState ? GPIO_PIN_SET : GPIO_PIN_RESET);
}


/* Sets SPI SCK and MOSI in work (low) state before exchange. */
void BOARD_SPI_IdleTransmit(void)
{
    static uint8_t ignored = 0x00;
    
    if (HAL_SPI_TransmitReceive(&hspi1, &ignored, &ignored, 1, HAL_MAX_DELAY) != HAL_OK)
    {
        Error_Handler();
    }
}


static NRF24_ErrorTypeDef BOARD_TransmitReceiveSync(uint8_t *pTxRxData, uint16_t length, bool txFlag, bool rxFlag)
{
    if (length > NRF24_MAX_DATA_LENGTH)
    {
        return NRF24_ERR_HAL;
    }
    
    if (txFlag)
    {    
        memcpy(_spiTxAlignedBuffer, pTxRxData, length);
    }
    
    if (HAL_SPI_TransmitReceive(&hspi1, _spiTxAlignedBuffer, _spiRxAlignedBuffer, length, HAL_MAX_DELAY) != HAL_OK)
    {
        return NRF24_ERR_HAL;
    }
    
    if (rxFlag)
    {
        memcpy(pTxRxData, _spiRxAlignedBuffer, length);
    }
    
    return NRF24_ERR_OK;
}

/* Weak callback. */
NRF24_ErrorTypeDef NRF24_HAL_TransmitReceiveSync(void *pHalContext, uint8_t *pTxRxData, uint16_t length)
{
    return BOARD_TransmitReceiveSync(pTxRxData, length, true, true);
}

/* Weak callback. */
NRF24_ErrorTypeDef NRF24_HAL_TransmitSync(void *pHalContext, uint8_t *pTxData, uint16_t length)
{
    return BOARD_TransmitReceiveSync(pTxData, length, true, false);
}

/* Weak callback. */
NRF24_ErrorTypeDef NRF24_HAL_ReceiveSync(void *pHalContext, uint8_t *pRxData, uint16_t length)
{
    return BOARD_TransmitReceiveSync(pRxData, length, false, true);
}

/* Weak callback. */
NRF24_ErrorTypeDef NRF24_HAL_TakeControl(void *pHalContext)
{    
    HAL_GPIO_WritePin(NRF24_NSS_GPIO_Port, NRF24_NSS_Pin, GPIO_PIN_RESET); /* Active low */
    
    return NRF24_ERR_OK;
}

/* Weak callback. */
NRF24_ErrorTypeDef NRF24_HAL_ReleaseControl(void *pHalContext)
{
    HAL_GPIO_WritePin(NRF24_NSS_GPIO_Port, NRF24_NSS_Pin, GPIO_PIN_SET); /* Active low */
    
    return NRF24_ERR_OK;
}

/* Weak callback. */
NRF24_ErrorTypeDef NRF24_HAL_ActivateCe(void *pHalContext)
{
    HAL_GPIO_WritePin(NRF24_CE_GPIO_Port, NRF24_CE_Pin, GPIO_PIN_SET); /* Active high */
    
    return NRF24_ERR_OK;
}

/* Weak callback. */
NRF24_ErrorTypeDef NRF24_HAL_DeactivateCe(void *pHalContext)
{
    HAL_GPIO_WritePin(NRF24_CE_GPIO_Port, NRF24_CE_Pin, GPIO_PIN_RESET); /* Active high */
    
    return NRF24_ERR_OK;
}

/* Weak callback. */
NRF24_ErrorTypeDef NRF24_HAL_IsIrq(void *pHalContext, bool *pFlag)
{
    *pFlag = HAL_GPIO_ReadPin(NRF24_IRQ_GPIO_Port, NRF24_IRQ_Pin) == GPIO_PIN_RESET; /* Active low, logical state inversion */
    
    return NRF24_ERR_OK;
}

/* Weak callback. */
void MX_USB_DEVICE_HardwareStart(void)
{
    HAL_GPIO_WritePin(USBCTRL_GPIO_Port, USBCTRL_Pin, GPIO_PIN_SET);
}
