#ifndef __BOARD_H
#define __BOARD_H


#include <stdbool.h>


void BOARD_SetTxLedState        (bool txLedState);
void BOARD_SetRxLedState        (bool rxLedState);
void BOARD_SPI_IdleTransmit     (void);


#endif /* __BOARD_H */
