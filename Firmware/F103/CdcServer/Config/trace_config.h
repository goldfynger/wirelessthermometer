#ifndef __TRACE_CONFIG_H
#define __TRACE_CONFIG_H


/* Allow trace via SWO. */
#define TRACE_CONFIG_SWO

/* Allow trace via UART. */
#define TRACE_CONFIG_SERIAL

/* UART handle that used for trace. */
#define TRACE_CONFIG_UART_HANDLE            huart1

/* Add incremented number before each string. */
#define TRACE_CONFIG_ADD_STRING_NUMBER

/* Use atomic printf if OS2 not used. */
//#define TRACE_CONFIG_ATOMIC

/* Trace using OS mechanisms. */
#define TRACE_CONFIG_OS2

/* Trace task handle. */
#define TRACE_CONFIG_TASK_HANDLE            traceTaskHandle

/* Trace queue handle. */
#define TRACE_CONFIG_QUEUE_HANDLE           traceQueueHandle

/* Trace task state. */
#define TRACE_CONFIG_TASK_STATE             traceTaskState

/* Default size of memory allocation to write format message. */
#define TRACE_CONFIG_FORMAT_DEFAULT_SIZE    128U

/* Maximum size of memory allocation to write format message. If not equal to TRACE_CONFIG_FORMAT_DEFAULT_SIZE then reallocation allowed. */
#define TRACE_CONFIG_FORMAT_MAX_SIZE        128U

/* Size of OS queue to transmit trace messages. */
#define TRACE_CONFIG_QUEUE_COUNT            4U


#endif /* __TRACE_CONFIG_H */
