#ifndef __NRF24_CONFIG_H
#define __NRF24_CONFIG_H


#include <stdbool.h>
#include <stdint.h>
#include "nrf24.h"


#define NRF24_SETUP_RX_TX_MODE                  (NRF24_RX_TX_MODE_RX_ONLY)                                      /* Default: (NRF24_RX_TX_MODE_RX_TX) */
#define NRF24_SETUP_RX_DATA_PIPE                ((NRF24_RxDataPipesTypeDef)(NRF24_RX_DATA_PIPE_2 | NRF24_RX_DATA_PIPE_3 | NRF24_RX_DATA_PIPE_4 | NRF24_RX_DATA_PIPE_5))
                                                                                                                /* Default: (NRF24_RX_DATA_PIPE_1) */
#define NRF24_SETUP_FREQUENCY_CHANNEL           ((uint8_t)32)                                                   /* Default: ((uint8_t)2) */
//#define NRF24_SETUP_RETR_DELAY                  (NRF24_RETR_DELAY_250)                                          /* Default: (NRF24_RETR_DELAY_250) */
//#define NRF24_SETUP_RETR_COUNT                  (NRF24_RETR_COUNT_UP_TO_3)                                      /* Default: (NRF24_RETR_COUNT_UP_TO_3) */
//#define NRF24_SETUP_RETR_SETUP                  ((uint8_t)(NRF24_SETUP_RETR_DELAY | NRF24_SETUP_RETR_COUNT))    /* Default: ((uint8_t)(NRF24_SETUP_RETR_DELAY | NRF24_SETUP_RETR_COUNT)) */
//#define NRF24_SETUP_DATA_RATE                   (NRF24_DATA_RATE_2MBPS)                                         /* Default: (NRF24_DATA_RATE_2MBPS) */
//#define NRF24_SETUP_AMP_POWER                   (NRF24_AMP_POWER_0DBM)                                          /* Default: (NRF24_AMP_POWER_0DBM) */
//#define NRF24_SETUP_RATE_POWER_SETUP            ((uint8_t)(NRF24_SETUP_DATA_RATE | NRF24_SETUP_AMP_POWER))      /* Default: ((uint8_t)(NRF24_SETUP_DATA_RATE | NRF24_SETUP_AMP_POWER)) */
//#define NRF24_SETUP_TX_RX0_ADDRESS_B0           ((uint8_t)0xE7)                                                 /* Default: ((uint8_t)0xE7) */
//#define NRF24_SETUP_TX_RX0_ADDRESS_B1           ((uint8_t)0xE7)                                                 /* Default: ((uint8_t)0xE7) */
//#define NRF24_SETUP_TX_RX0_ADDRESS_B2           ((uint8_t)0xE7)                                                 /* Default: ((uint8_t)0xE7) */
//#define NRF24_SETUP_TX_RX0_ADDRESS_B3           ((uint8_t)0xE7)                                                 /* Default: ((uint8_t)0xE7) */
//#define NRF24_SETUP_TX_RX0_ADDRESS_B4           ((uint8_t)0xE7)                                                 /* Default: ((uint8_t)0xE7) */
#define NRF24_SETUP_RX1_ADDRESS_B0              ((uint8_t)0x32)                                                 /* Default: ((uint8_t)0xC2) */
#define NRF24_SETUP_RX1_ADDRESS_B1              ((uint8_t)0x32)                                                 /* Default: ((uint8_t)0xC2) */
#define NRF24_SETUP_RX1_ADDRESS_B2              ((uint8_t)0x32)                                                 /* Default: ((uint8_t)0xC2) */
#define NRF24_SETUP_RX1_ADDRESS_B3              ((uint8_t)0x32)                                                 /* Default: ((uint8_t)0xC2) */
#define NRF24_SETUP_RX1_ADDRESS_B4              ((uint8_t)0x32)                                                 /* Default: ((uint8_t)0xC2) */
#define NRF24_SETUP_RX2_ADDRESS_B0              ((uint8_t)0x33) /* 33..36 for F030F4 sensors */                 /* Default: ((uint8_t)0xC3) */
#define NRF24_SETUP_RX3_ADDRESS_B0              ((uint8_t)0x34)                                                 /* Default: ((uint8_t)0xC4) */
#define NRF24_SETUP_RX4_ADDRESS_B0              ((uint8_t)0x35)                                                 /* Default: ((uint8_t)0xC5) */
#define NRF24_SETUP_RX5_ADDRESS_B0              ((uint8_t)0x36)                                                 /* Default: ((uint8_t)0xC6) */
//#define NRF24_SETUP_POWER_DOWN_IN_TX_ONLY_MODE  (true)                                                          /* Default: (true) */


#endif /* __NRF24_CONFIG_H */
