#ifndef __CMSIS_OS2_APP_H
#define __CMSIS_OS2_APP_H


#include "stm32f1xx_hal.h"


__NO_RETURN void CMSIS_OS2_APP_Start(void);


#endif /* __CMSIS_OS2_APP_H */
