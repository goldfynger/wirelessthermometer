#ifndef __CDC_APP_H
#define __CDC_APP_H


#include <stdbool.h>
#include <stdint.h>
#include "universal_device.h"
#include "stm32f1xx_hal.h"


#define CDC_APP_IN_QUEUE_COUNT  4
#define CDC_APP_OUT_QUEUE_COUNT 4


typedef struct
{
    uint8_t Data[UNIVERSAL_DEVICE_MESSAGE_SIZE];
}
CDC_APP_QueueMessageTypeDef;


__NO_RETURN void    CDC_APP_Task            (void *argument);
bool                CDC_APP_PostInQueue     (CDC_APP_QueueMessageTypeDef *pMessage);
bool                CDC_APP_GetFromQueue    (CDC_APP_QueueMessageTypeDef *pMessage);


#endif /* __CDC_APP_H */
