#include <stdbool.h>
#include <stdint.h>
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"
#include "trace.h"
#include "trace_config.h"
#include "indication.h"
#include "cdc_app.h"
#include "server_app.h"
#include "board.h"
#include "cmsis_os2_app.h"


osThreadId_t traceTaskHandle;
const osThreadAttr_t _traceTask_attributes = {
  .name = "traceTask",
  .priority = (osPriority_t) osPriorityNormal7,
  .stack_size = 128 * 4
};

osThreadId_t cdcTaskHandle;
const osThreadAttr_t _cdcTask_attributes = {
  .name = "cdcTask",
  .priority = (osPriority_t) osPriorityNormal5,
  .stack_size = 128 * 4
};

osThreadId_t serverTaskHandle;
const osThreadAttr_t _serverTask_attributes = {
  .name = "serverTask",
  .priority = (osPriority_t) osPriorityNormal3,
  .stack_size = 192 * 4
};


osMessageQueueId_t traceQueueHandle;
const osMessageQueueAttr_t _traceQueue_attributes = {
  .name = "traceQueue"
};

osMessageQueueId_t cdcInQueueHandle;
const osMessageQueueAttr_t _cdcInQueue_attributes = {
  .name = "cdcInQueue"
};

osMessageQueueId_t cdcOutQueueHandle;
const osMessageQueueAttr_t _cdcOutQueue_attributes = {
  .name = "cdcOutQueue"
};


osTimerId_t rxTxIndicationTimerHandle;
const osTimerAttr_t _rxTxIndicationTimer_attributes = {
  .name = "rxTxTimer"
};


CMSIS_OS2_EX_TaskStateTypeDef traceTaskState = CMSIS_OS2_EX_TASK_STATE_IDLE;
CMSIS_OS2_EX_TaskStateTypeDef cdcTaskState = CMSIS_OS2_EX_TASK_STATE_IDLE;
CMSIS_OS2_EX_TaskStateTypeDef serverTaskState = CMSIS_OS2_EX_TASK_STATE_IDLE;


__NO_RETURN void CMSIS_OS2_APP_Start(void)
{
    osKernelInitialize();
    
    
    traceQueueHandle = osMessageQueueNew(TRACE_CONFIG_QUEUE_COUNT, sizeof(TRACE_QueueMessageTypeDef), &_traceQueue_attributes);
    
    cdcInQueueHandle = osMessageQueueNew(CDC_APP_IN_QUEUE_COUNT, sizeof(CDC_APP_QueueMessageTypeDef), &_cdcInQueue_attributes);
    
    cdcOutQueueHandle = osMessageQueueNew(CDC_APP_OUT_QUEUE_COUNT, sizeof(CDC_APP_QueueMessageTypeDef), &_cdcOutQueue_attributes);
    
    
    traceTaskHandle = osThreadNew(TRACE_Task, NULL, &_traceTask_attributes);
    
    cdcTaskHandle = osThreadNew(CDC_APP_Task, NULL, &_cdcTask_attributes);
    
    serverTaskHandle = osThreadNew(SERVER_APP_Task, NULL, &_serverTask_attributes);
    
    
    rxTxIndicationTimerHandle = osTimerNew(INDICATION_TimerCallback, osTimerPeriodic, NULL, &_rxTxIndicationTimer_attributes);
    
    osTimerStart(rxTxIndicationTimerHandle, INDICATION_TIMER_PERIOD_MS);
        
    
    osKernelStart();
    
    
    /* Should never get here. */
    while (true)
    {
    }
}
