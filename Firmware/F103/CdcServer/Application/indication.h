#ifndef __INDICATION_H
#define __INDICATION_H


#include <stdint.h>


#define INDICATION_MAX_RX_EVENTS    4
#define INDICATION_MAX_TX_EVENTS    4
#define INDICATION_TIMER_PERIOD_MS  50


void INDICATION_TimerCallback   (void *argument);
void INDICATION_RxEvent         (void);
void INDICATION_TxEvent         (void);


#endif /* __INDICATION_H */
