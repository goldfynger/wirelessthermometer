#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "nrf24.h"
#include "nrf24_setup.h"
#include "nrf24_app.h"


static NRF24_HandleTypeDef _nrf24Handle;


static bool NRF24_APP_Init(void);


bool NRF24_APP_ReceivePacket(WIRELESS_THERMOMETER_PacketTypeDef *pPacket)
{
    bool isInitialized = false;
    
    if (NRF24_IsInitialized(&_nrf24Handle, &isInitialized) != NRF24_ERR_OK)
    {
        return false;
    }
    
    if (!isInitialized)
    {
        if (!NRF24_APP_Init())
        {
            return false;
        }
    }
    
    
    if (NRF24_BeginReceive(&_nrf24Handle, (uint8_t *)pPacket, sizeof(WIRELESS_THERMOMETER_PacketTypeDef)) != NRF24_ERR_OK)
    {
        return false;
    }
    
    
    while (true)
    {    
        uint8_t pipe = 0;
        uint8_t length = 0;
        
        
        NRF24_ErrorTypeDef rxError = NRF24_EndReceive(&_nrf24Handle, &pipe, &length);
        
        switch (rxError)
        {            
            case NRF24_ERR_RX_COMPLETE:
                return true;
            
            case NRF24_ERR_RX_NOT_COMPLETE:
                continue;
            
            default:
                return false;
        }
    }
}


static bool NRF24_APP_Init(void)
{
    bool isInitialized = false;
    
    if (NRF24_IsInitialized(&_nrf24Handle, &isInitialized) != NRF24_ERR_OK)
    {
        return false;
    }
    
    if (isInitialized && NRF24_DeInit(&_nrf24Handle) != NRF24_ERR_OK)
    {
        return false;
    }
    
    memset(&_nrf24Handle, 0, sizeof(NRF24_HandleTypeDef)); /* If reused. */
    
    
    if (NRF24_SETUP_SetPreselectedConfig(&_nrf24Handle) != NRF24_ERR_OK)
    {
        return false;
    }
    
    
    if (NRF24_Init(&_nrf24Handle) != NRF24_ERR_OK)
    {
        return false;
    }
    
    
    return true;
}
