#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"
#include "trace.h"
#include "usb_cdc_api.h"
#include "cdc_app.h"


/* Kernel objects. */
extern osThreadId_t cdcTaskHandle;

extern osMessageQueueId_t cdcInQueueHandle;
extern osMessageQueueId_t cdcOutQueueHandle;

extern CMSIS_OS2_EX_TaskStateTypeDef cdcTaskState;
/* Kernel objects. */

/* Buffer and index variables MUST be used only in USB_CDC_API_ReceiveCompleteCallback(). */
static CDC_APP_QueueMessageTypeDef _rxMessageBuffer = {0};
static uint8_t _rxMessageBufferIdx = 0;


static void CDC_APP_ErrorHandler(uint32_t line);


__NO_RETURN void CDC_APP_Task(void *argument)
{
    TRACE_Format("%s started.\r\n", __FUNCTION__);
    
    cdcTaskState = CMSIS_OS2_EX_TASK_STATE_START;
    
    USB_CDC_API_Initialize();
    
    while (!USB_CDC_API_IsConnected())
    {
        osDelay(10);
    }
    
    TRACE_Format("%s connected.\r\n", __FUNCTION__);
    
    cdcTaskState = CMSIS_OS2_EX_TASK_STATE_RUN;
    
    
    static CDC_APP_QueueMessageTypeDef itemToTransmit;
    
    
    while(true)
    {
        if (osExMessageQueueGetWait(cdcInQueueHandle, &itemToTransmit) != osOK)
        {
            CDC_APP_ErrorHandler(__LINE__);
        }
                                
        while(USB_CDC_API_IsTransmitBusy())
        {
            osDelay(1);
        }
        
        USB_CDC_API_Transmit(itemToTransmit.Data, UNIVERSAL_DEVICE_MESSAGE_SIZE);
    }
}

/* Posts message in USB CDC queue. */
bool CDC_APP_PostInQueue(CDC_APP_QueueMessageTypeDef *pMessage)
{
    if (pMessage == NULL)
    {
        CDC_APP_ErrorHandler(__LINE__);
    }
    
    osStatus_t result = osExMessageQueuePutTry(cdcInQueueHandle, pMessage);
    
    if (result == osOK)
    {
        return true;
    }
    else if (result == osErrorResource)
    {
        return false;
    }
    else
    {
        CDC_APP_ErrorHandler(__LINE__);
        return false;
    }
}

/* Gets mesage from USB CDC queue. */
bool CDC_APP_GetFromQueue(CDC_APP_QueueMessageTypeDef *pMessage)
{
    if (pMessage == NULL)
    {
        CDC_APP_ErrorHandler(__LINE__);
    }
    
    osStatus_t result = osExMessageQueueGetTry(cdcOutQueueHandle, pMessage);
    
    if (result == osOK)
    {
        return true;
    }
    else if (result == osErrorResource)
    {
        return false;
    }
    else
    {
        CDC_APP_ErrorHandler(__LINE__);
        return false;
    }
}

static void CDC_APP_ErrorHandler(uint32_t line)
{
    const char *errorFormat = "%s on line %u.\r\n";
    
    if (osThreadGetId() == cdcTaskHandle)
    {
        cdcTaskState = CMSIS_OS2_EX_TASK_STATE_ERROR;
        
        TRACE_Format(errorFormat, __FUNCTION__, line);
        
        while(true)
        {
            osDelay(osWaitForever);
        }
    }
    else
    {
        oxExDisableInterrupts();
        
        printf(errorFormat, __FUNCTION__, line);
        
        while (true)
        {
        }
    }
}


/* Weak callback. */
void USB_CDC_API_ReceiveCompleteCallback(uint8_t *pBuffer, uint16_t length)
{
    while (length)
    {
        uint8_t bytesToCopy = (length + _rxMessageBufferIdx) <= UNIVERSAL_DEVICE_MESSAGE_SIZE ? length : (UNIVERSAL_DEVICE_MESSAGE_SIZE - _rxMessageBufferIdx);
        
        memcpy(_rxMessageBuffer.Data + _rxMessageBufferIdx, pBuffer, bytesToCopy);
        
        _rxMessageBufferIdx += bytesToCopy;
        length -= bytesToCopy;
        
        if (_rxMessageBufferIdx == UNIVERSAL_DEVICE_MESSAGE_SIZE)
        {
            if (osExMessageQueuePutTry(cdcOutQueueHandle, _rxMessageBuffer.Data) != osOK)
            {
                CDC_APP_ErrorHandler(__LINE__);
            }
            
            _rxMessageBufferIdx = 0;
        }
    }
}
