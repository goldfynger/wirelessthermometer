#include <stdbool.h>
#include <stdint.h>
#include "cmsis_os2_ex.h"
#include "board.h"
#include "indication.h"


static uint8_t _rxEventCounter = 0;
static uint8_t _txEventCounter = 0;


void INDICATION_TimerCallback(void *argument)
{
    static bool isRxTime = false;
    
    if (isRxTime)
    {
        bool isRx = false;
        
        osExEnterCritical();
        
        if (_rxEventCounter)
        {
            isRx = true;
            
            _rxEventCounter -= 1;
        }
        
        __force_stores();
        osExExitCritical();
        
        BOARD_SetRxLedState(isRx);
        BOARD_SetTxLedState(false);        
        
        isRxTime = false; /* Next time for TX. */
    }
    else
    {
        bool isTx = false;
        
        osExEnterCritical();
        
        if (_txEventCounter)
        {
            isTx = true;
            
            _txEventCounter -= 1;
        }
        
        __force_stores();
        osExExitCritical();
        
        BOARD_SetRxLedState(false);
        BOARD_SetTxLedState(isTx);        
        
        isRxTime = true; /* Next time for RX. */
    }
}

void INDICATION_RxEvent(void)
{
    osExEnterCritical();
    
    if (_rxEventCounter < INDICATION_MAX_RX_EVENTS)
    {
        _rxEventCounter += 1;
    }
    
    __force_stores();
    osExExitCritical();
}

void INDICATION_TxEvent(void)
{
    osExEnterCritical();
    
    if (_txEventCounter < INDICATION_MAX_TX_EVENTS)
    {
        _txEventCounter += 1;
    }
    
    __force_stores();
    osExExitCritical();
}
