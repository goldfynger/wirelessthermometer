#ifndef __SERVER_APP_H
#define __SERVER_APP_H


#include "stm32f1xx_hal.h"


__NO_RETURN void SERVER_APP_Task(void *argument);


#endif /* __SERVER_APP_H */
