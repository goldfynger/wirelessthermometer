#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"
#include "trace.h"
#include "cdc_app.h"
#include "universal_device.h"
#include "server_app.h"


/* Kernel objects. */
extern osThreadId_t serverTaskHandle;

extern CMSIS_OS2_EX_TaskStateTypeDef serverTaskState;
/* Kernel objects. */


static void SERVER_APP_ErrorHandler(uint32_t line);


__NO_RETURN void SERVER_APP_Task(void *argument)
{
    TRACE_Format("%s started.\r\n", __FUNCTION__);
    
    serverTaskState = CMSIS_OS2_EX_TASK_STATE_START;
    
    if (UNIVERSAL_DEVICE_Initialise() != UNIVERSAL_DEVICE_ERR_OK)
    {
        SERVER_APP_ErrorHandler(__LINE__);
    }
    
    TRACE_Format("%s initialised.\r\n", __FUNCTION__);
    
    serverTaskState = CMSIS_OS2_EX_TASK_STATE_RUN;
    
    
    while(true)
    {
        CDC_APP_QueueMessageTypeDef *pCdcMessage = osExMalloc(sizeof(CDC_APP_QueueMessageTypeDef));
        
        if (pCdcMessage == NULL)
        {
            SERVER_APP_ErrorHandler(__LINE__);
        }
        
        while (!CDC_APP_GetFromQueue(pCdcMessage))
        {
            osDelay(1);
        }
        
        
        UNIVERSAL_DEVICE_MessageTypeDef *pResponseMessages;
        uint8_t responseCount;
        
        if (UNIVERSAL_DEVICE_ProcessMessage((UNIVERSAL_DEVICE_MessageTypeDef *)pCdcMessage, &pResponseMessages, &responseCount) != UNIVERSAL_DEVICE_ERR_OK)
        {
            SERVER_APP_ErrorHandler(__LINE__);
        }
        
        
        for (uint8_t responseIndex = 0; responseIndex < responseCount; responseIndex++)
        {
            while (!CDC_APP_PostInQueue((CDC_APP_QueueMessageTypeDef *)(pResponseMessages + responseIndex)))
            {
                osDelay(1);
            }
            
            osExFree((CDC_APP_QueueMessageTypeDef *)(pResponseMessages + responseIndex));
        }
    }
}

static void SERVER_APP_ErrorHandler(uint32_t line)
{
    const char *errorFormat = "%s on line %u.\r\n";
    
    if (osThreadGetId() == serverTaskHandle)
    {
        serverTaskState = CMSIS_OS2_EX_TASK_STATE_ERROR;
        
        TRACE_Format(errorFormat, __FUNCTION__, line);
        
        while(true)
        {
            osDelay(osWaitForever);
        }
    }
    else
    {
        oxExDisableInterrupts();
        
        printf(errorFormat, __FUNCTION__, line);
        
        while (true)
        {
        }
    }    
}


/* Weak callback. */
UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_InitiativeMessageCallback(UNIVERSAL_DEVICE_MessageTypeDef *pInitiativeMessages, uint8_t initiativeCount)
{
    for (uint8_t initiativeIndex = 0; initiativeIndex < initiativeCount; initiativeIndex++)
    {
        while (!CDC_APP_PostInQueue((CDC_APP_QueueMessageTypeDef *)(pInitiativeMessages + initiativeIndex)))
        {
            osDelay(1);
        }
    }
    
    return UNIVERSAL_DEVICE_ERR_OK;
}
