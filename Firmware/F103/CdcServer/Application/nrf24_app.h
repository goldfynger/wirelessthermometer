#ifndef __NRF24_APP_H
#define __NRF24_APP_H


#include <stdbool.h>
#include <stdint.h>
#include <time.h>
#include "device_info.h"


typedef struct
{
    DEVICE_INFO_InformationTypeDef  DeviceInformation;
    DEVICE_INFO_UidTypeDef          DeviceUid;
    
    time_t                          UpTime;
    uint32_t                        Temperature;
    uint32_t                        ChipTemperature;
    uint16_t                        ChipVoltage;
}
WIRELESS_THERMOMETER_PacketTypeDef;


bool NRF24_APP_ReceivePacket(WIRELESS_THERMOMETER_PacketTypeDef *pPacket);


#endif /* __NRF24_APP_H */
