#ifndef __MEASUREMENT_LOW_POWER_H
#define __MEASUREMENT_LOW_POWER_H


#include "stm32f0xx_hal.h"


__NO_RETURN void MEASUREMENT_LOW_POWER_Hook(void);


#endif /* __MEASUREMENT_LOW_POWER_H */
