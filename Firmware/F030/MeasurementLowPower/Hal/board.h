#ifndef __BOARD_H
#define __BOARD_H


#include <stdbool.h>
#include <stdint.h>
#include <time.h>
#include "stm32f0xx_hal.h"


void                BOARD_Init              (void);
void                BOARD_GetDateTime       (struct tm *pRtcDateTime);
time_t              BOARD_GetUpTime         (void);
void                BOARD_ReadChipState     (int32_t *pChipTemperature, uint16_t *pChipVoltage);
void                BOARD_Delay             (uint16_t secondsTotal);
void                BOARD_Sleep             (uint16_t secondsTotal);
void                BOARD_Stop              (uint16_t secondsTotal);
__NO_RETURN void    BOARD_Standby           (uint16_t secondsTotal);
void                BOARD_RefreshWatchdog   (void);
void                BOARD_SuspendWatchdog   (void);
void                BOARD_ResumeWatchdog    (void);



#endif /* __BOARD_H */
