#include <stdbool.h>
#include <stdint.h>
#include <time.h>
#include "trace.h"
#include "stm32f0xx_hal.h"
#include "adc.h"
#include "gpio.h"
#include "spi.h"
#include "rtc.h"
#include "usart.h"
#include "wwdg.h"
#include "main.h"
#include "board.h"


#define TS_CAL1             (*((volatile uint16_t *)(0x1FFFF7B8)))  /* Stored ADC 12-bit value on 30�C and 3300 mV VCC (ADC). */
#define VREFINT_CAL         (*((volatile uint16_t *)(0x1FFFF7BA)))  /* Stored ADC 12-bit value on 3300 mV VCC (ADC). */
#define VOLTAGE_CAL         ((uint16_t)3300)                        /* Calibration voltage (mV). */
#define ADC_MAX             ((uint16_t)4095)                        /* In 12-bit mode (ADC). */
#define TS_AVG_SLOPE        ((uint16_t)4300)                        /* Datasheet typical value (�V/�C) */
#define TS_AVG_SLOPE_ADC    ((uint16_t)5336)                        /* ((TS_AVG_SLOPE * ADC_MAX) / VOLTAGE_CAL) (mADC/(�C)) */


extern void SystemClock_Config(void);

static uint16_t BOARD_CalibrateAdc(void);
static uint16_t BOARD_ReadAdcSync(void);
static uint16_t CalculateChipVoltage(uint16_t chipVoltageAdc);
static uint16_t CalculateChipTemperature(uint16_t chipTemperatureAdc, uint16_t chipVoltageAdc);
static void BOARD_SetupNextAlarm(uint16_t secondsTotal);


void BOARD_Init(void)
{
    if (HAL_Init() != HAL_OK)
    {
        Error_Handler();
    }
    
    SystemClock_Config();
    
    bool lowPowerReset = __HAL_RCC_GET_FLAG(RCC_FLAG_LPWRRST) != RESET;
    bool wwdgReset = __HAL_RCC_GET_FLAG(RCC_FLAG_WWDGRST) != RESET;
    bool iwdgReset = __HAL_RCC_GET_FLAG(RCC_FLAG_IWDGRST) != RESET;
    bool softwareReset = __HAL_RCC_GET_FLAG(RCC_FLAG_SFTRST) != RESET;
    bool porReset = __HAL_RCC_GET_FLAG(RCC_FLAG_PORRST) != RESET;
    bool pinReset = __HAL_RCC_GET_FLAG(RCC_FLAG_PINRST) != RESET;
    
    __HAL_RCC_CLEAR_RESET_FLAGS();
    
    bool standbyFlag = __HAL_PWR_GET_FLAG(PWR_FLAG_SB) != RESET;
    
    /* If reset from STANDBY mode, clear SB flag. */
    if (standbyFlag)
    {
        __HAL_PWR_CLEAR_FLAG(PWR_FLAG_SB);
    }
    
    MX_GPIO_Init();
    MX_USART1_UART_Init(); /* Initialized first for trace. */
    
    /* Do not init RTC if reset caused by WWDG or STANDBY mode. */
    if (wwdgReset || standbyFlag)
    {
        hrtc.Instance = RTC;
        hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
        hrtc.Init.AsynchPrediv = 127;
        hrtc.Init.SynchPrediv = 311;
        hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
        hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
        hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
        
        if (HAL_RTC_Init(&hrtc) != HAL_OK)
        {
            Error_Handler();
        }
        
        /* Wait for copy RTC values in shadow register. */
        if (HAL_RTC_WaitForSynchro(&hrtc) != HAL_OK)
        {
            Error_Handler();
        }
        
        TRACE_Format("%010u: Wake up from STANDBY.\r\n", BOARD_GetUpTime());
    }
    /* Else set initial RTC values. */
    else
    {
        MX_RTC_Init();
    }
    
    TRACE_Format("%010u: Reset causes: (LPWRRST:%u)(WWDGRST:%u)(IWDGRST:%u)(SFTRST:%u)(PORRST:%u)(PINRST:%u).\r\n", BOARD_GetUpTime(), lowPowerReset, wwdgReset, iwdgReset, softwareReset, porReset, pinReset);
    
    /* Switch to error handler if reset caused by WWDG. */
    if (wwdgReset)
    {
        Error_Handler();
    }
        
    MX_SPI1_Init();
    MX_ADC_Init();
    MX_WWDG_Init();
}

void BOARD_GetDateTime(struct tm *pRtcDateTime)
{
    RTC_TimeTypeDef rtcTime = {0};
    RTC_DateTypeDef rtcDate = {0};
        
    if (HAL_RTC_GetTime(&hrtc, &rtcTime, RTC_FORMAT_BIN) != HAL_OK) /* Time before date. Date locks in shadow register. */
    {
        Error_Handler();
    }
    
    if (HAL_RTC_GetDate(&hrtc, &rtcDate, RTC_FORMAT_BIN) != HAL_OK)
    {
        Error_Handler();
    }
    
    pRtcDateTime->tm_year = rtcDate.Year + 100;
    if (rtcDate.Month < 0x10) // Month in BCD
    {
        pRtcDateTime->tm_mon = rtcDate.Month - 1;
    }
    else
    {
        pRtcDateTime->tm_mon = rtcDate.Month - 7;
    }
    pRtcDateTime->tm_mday = rtcDate.Date;
    pRtcDateTime->tm_hour = rtcTime.Hours;
    pRtcDateTime->tm_min = rtcTime.Minutes;
    pRtcDateTime->tm_sec = rtcTime.Seconds;
}

time_t BOARD_GetUpTime(void)
{
    struct tm rtcDateTime = {0};
    
    BOARD_GetDateTime(&rtcDateTime);
    
    time_t upTime = mktime(&rtcDateTime);
    
    if (upTime == (time_t)-1)
    {
        Error_Handler();
    }
    
    return upTime - 946684800; /* Result is count of seconds from 2000.01.01T00:00:00 */
}

void BOARD_ReadChipState(int32_t *pChipTemperature, uint16_t *pChipVoltage)
{
    BOARD_CalibrateAdc();
    
    uint16_t chipTemperatureAdc = BOARD_ReadAdcSync();
        
    uint16_t chipVoltageAdc = BOARD_ReadAdcSync();
    
    
    *pChipTemperature = CalculateChipTemperature(chipTemperatureAdc, chipVoltageAdc);
    
    *pChipVoltage = CalculateChipVoltage(chipVoltageAdc);
}

void BOARD_Delay(uint16_t secondsTotal)
{
    BOARD_RefreshWatchdog();
    
    uint32_t tenMsTotal = secondsTotal * 100;
    
    for (uint32_t times = 0; times < tenMsTotal; times++)
    {
        HAL_Delay(10);
        
        BOARD_RefreshWatchdog();
    }
}

void BOARD_Sleep(uint16_t secondsTotal)
{
    BOARD_SetupNextAlarm(secondsTotal);
    
    HAL_SuspendTick(); /* Disable systick interrupts. */

    HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
    
    HAL_ResumeTick();
    
    BOARD_SetupNextAlarm(0); /* Wait for 24 hours. */
}

void BOARD_Stop(uint16_t secondsTotal)
{
    BOARD_SetupNextAlarm(secondsTotal);
    
    HAL_SuspendTick(); /* Disable systick interrupts. */
    
    HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI);
    
    HAL_ResumeTick();
    
    SystemClock_Config();  /* Re-setup system clock. */
    
    BOARD_SetupNextAlarm(0); /* Wait for 24 hours. */
}

__NO_RETURN void BOARD_Standby(uint16_t secondsTotal)
{
    BOARD_SetupNextAlarm(secondsTotal);
    
    __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU); /* Clear wake-up flag; otherwise alarm fired immediately. */
    
    HAL_PWR_EnterSTANDBYMode();
    
    while (true)
    {
    }
}

void BOARD_RefreshWatchdog(void)
{
    HAL_WWDG_Refresh(&hwwdg);
}

void BOARD_SuspendWatchdog(void)
{
    __HAL_RCC_WWDG_CLK_DISABLE();
}

void BOARD_ResumeWatchdog(void)
{
    __HAL_RCC_WWDG_CLK_ENABLE();
}


static uint16_t BOARD_CalibrateAdc(void)
{
    if (HAL_ADCEx_Calibration_Start(&hadc) != HAL_OK)
    {
        Error_Handler();
    }
    
    /* Return calibration factor. */
    return HAL_ADC_GetValue(&hadc);
}

static uint16_t BOARD_ReadAdcSync(void)
{
    /* Used discontinuous mode - each conversion of sequense starts manually. */
    
    if (HAL_ADC_Start(&hadc) != HAL_OK)
    {
        Error_Handler();
    }
    
    if (HAL_ADC_PollForConversion(&hadc, HAL_MAX_DELAY) != HAL_OK)
    {
        Error_Handler();
    }
    
    return HAL_ADC_GetValue(&hadc);
}

static uint16_t CalculateChipVoltage(uint16_t chipVoltageAdc)
{
    /* Vchip = (Vcal * ADCcal) / ADCmeasV */
    
    int32_t temp = 0;
    
    temp = VOLTAGE_CAL;         /* (mV) */
    temp *= VREFINT_CAL;        /* (mV * ADC) */
    temp /= chipVoltageAdc;     /* (((mV * ADC) / ADC) = mV) */
    return (uint16_t)temp;      /* (mV) */
}

static uint16_t CalculateChipTemperature(uint16_t chipTemperatureAdc, uint16_t chipVoltageAdc)
{
    /* Tchip = ((TS_CAL1 - ADCt) / AVG_SLOPE_ADC) + 30 */
    /* ADCt = (ADCmeasT * Vchip) / Vcal */
    
    int32_t temp = 0;
    
    temp = chipTemperatureAdc;                      /* (ADC) */
    temp *= CalculateChipVoltage(chipVoltageAdc);   /* (mV * ADC) */
    temp /= VOLTAGE_CAL;                            /* (((mV * ADC) / mV) = ADC) */
    temp = TS_CAL1 - temp;                          /* (ADC) */
    temp *= 100000;                                 /* (mADC * 10^2) */
    temp /= TS_AVG_SLOPE_ADC;                       /* (((mADC * 10^2) / mADC/�C) = (�C * 10^2) */
    temp += 3000;                                   /* (�C * 10^2) */
    temp *= 10;                                     /* (m�C) */
    return temp;                                    /* (m�C) */
}

static void BOARD_SetupNextAlarm(uint16_t secondsTotal)
{
    /* If secondsTotal == 0 alarm will be fired in current day time after 24 hours. */
    /* Take current time, add seconds before alarm and setup new alarm. */
    
    RTC_TimeTypeDef rtcTime = {0};
    RTC_DateTypeDef rtcDate = {0};
        
    if (HAL_RTC_GetTime(&hrtc, &rtcTime, RTC_FORMAT_BIN) != HAL_OK) /* Time before date. Date locks in shadow register. */
    {
        Error_Handler();
    }
    
    if (HAL_RTC_GetDate(&hrtc, &rtcDate, RTC_FORMAT_BIN) != HAL_OK)
    {
        Error_Handler();
    }
    
    
    uint16_t seconds = secondsTotal % 60;
    uint16_t minutesTotal = secondsTotal / 60;
    uint16_t minutes = minutesTotal % 60;
    uint16_t hours = minutesTotal / 60;
    
    
    uint8_t alarmSecondsWithOwerflow = rtcTime.Seconds + seconds;
    uint8_t alarmSeconds = alarmSecondsWithOwerflow % 60;
    uint8_t alarmAdditionalMinute = alarmSecondsWithOwerflow >= 60 ? 1 : 0;
    
    uint8_t alarmMinutesWithOwerflow = rtcTime.Minutes + minutes + alarmAdditionalMinute;
    uint8_t alarmMinutes = alarmMinutesWithOwerflow % 60;
    uint8_t alarmAdditionalHour = alarmMinutesWithOwerflow >= 60 ? 1 : 0;
    
    uint8_t alarmHoursWithOwerflow = rtcTime.Hours + hours + alarmAdditionalHour;
    uint8_t alarmHours = alarmHoursWithOwerflow % 24;
        
    TRACE_Format("Now + diff = alarm %u:%u:%u + %u:%u:%u = %u:%u:%u\r\n", rtcTime.Hours, rtcTime.Minutes, rtcTime.Seconds, hours, minutes, seconds, alarmHours, alarmMinutes, alarmSeconds);
    
    RTC_AlarmTypeDef rtcAlarm;    
    
    rtcAlarm.AlarmTime.Hours = alarmHours;
    rtcAlarm.AlarmTime.Minutes = alarmMinutes;
    rtcAlarm.AlarmTime.Seconds = alarmSeconds;
    rtcAlarm.AlarmTime.SubSeconds = rtcTime.SubSeconds;
    rtcAlarm.AlarmTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
    rtcAlarm.AlarmTime.StoreOperation = RTC_STOREOPERATION_RESET;
    rtcAlarm.AlarmMask = RTC_ALARMMASK_DATEWEEKDAY;
    rtcAlarm.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_NONE;
    rtcAlarm.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_DATE;
    rtcAlarm.AlarmDateWeekDay = 1;
    rtcAlarm.Alarm = RTC_ALARM_A;
    
    if (HAL_RTC_SetAlarm_IT(&hrtc, &rtcAlarm, RTC_FORMAT_BIN) != HAL_OK)
    {
        Error_Handler();
    }
}
