#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "onewire.h"
#include "ds18b20.h"
#include "device_info.h"
#include "nrf24.h"
#include "nrf24_setup.h"
#include "board.h"
#include "main.h"
#include "wireless_thermometer.h"


typedef struct
{
    DEVICE_INFO_InfoTypeDef DeviceInfo;
    DEVICE_INFO_UidTypeDef  DeviceUid;
    
    time_t                  UpTime;
    uint32_t                Temperature;
    uint32_t                ChipTemperature;
    uint16_t                ChipVoltage;
    BOARD_ResetCauseTypeDef ResetCause;
}
WIRELESS_THERMOMETER_PacketTypeDef;


__NO_RETURN void WIRELESS_THERMOMETER_Hook(void)
{
    BOARD_Init();
    
    BOARD_RefreshWatchdog();
    
    
    int32_t chipTemperature;
    uint16_t chipVoltage;
        
    BOARD_ReadChipState(&chipTemperature, &chipVoltage);
    
    BOARD_RefreshWatchdog();
    
    
    DS18B20_HandleTypeDef ds18b20Handle = {0};
    
    DS18B20_DataTypeDef data =
    {
        .Temperature = 0,        
        .AlarmTriggerH = DS18B20_MAX_ALARM_TRIGGER_H,
        .AlarmTriggerL = DS18B20_MIN_ALARM_TRIGGER_L,        
        .Resolution = DS18B20_RESOLUTION_12
    };
    
    if (DS18B20_WriteData(&ds18b20Handle, NULL, &data, NULL) != OW_ERR_OK)
    {
        Error_Handler();
    }
    
    if (DS18B20_RunConversion(&ds18b20Handle, NULL, NULL) != OW_ERR_OK)
    {
        Error_Handler();
    }
    
    BOARD_Stop(WIRELESS_THERMOMETER_CONVERSION_TIME); /* Wait for conversion. */
    
    BOARD_RefreshWatchdog();
    
    
    if (DS18B20_ReadData(&ds18b20Handle, NULL, &data, NULL) != OW_ERR_OK)
    {
        Error_Handler();
    }
    
    int32_t temperature = data.Temperature;
    
    BOARD_RefreshWatchdog();
    
    
    const DEVICE_INFO_InfoTypeDef *pDeviceInfo = DEVICE_INFO_GetInfo();
    const DEVICE_INFO_UidTypeDef *pDeviceUid = DEVICE_INFO_GetUid();
    
    time_t upTime = BOARD_GetUpTime();
    BOARD_ResetCauseTypeDef resetCause = BOARD_GetResetCause();
    
    WIRELESS_THERMOMETER_PacketTypeDef packet = 
    {
        .UpTime = upTime,
        .Temperature = temperature,
        .ChipTemperature = chipTemperature,
        .ChipVoltage = chipVoltage,
        .ResetCause = resetCause,
    };
    
    memcpy(&packet.DeviceInfo, pDeviceInfo, sizeof(DEVICE_INFO_InfoTypeDef));
    memcpy(&packet.DeviceUid, pDeviceUid, sizeof(DEVICE_INFO_UidTypeDef));
    
    BOARD_RefreshWatchdog();
    

    NRF24_HandleTypeDef nrf24Handle;
    memset(&nrf24Handle, 0, sizeof(NRF24_HandleTypeDef));
    
    if (NRF24_SETUP_SetPreselectedConfig(&nrf24Handle) != NRF24_ERR_OK)
    {
        Error_Handler();
    }
    
    if (NRF24_Init(&nrf24Handle) != NRF24_ERR_OK)
    {
        Error_Handler();
    }
    
    if (NRF24_BeginTransmit(&nrf24Handle, (uint8_t *)&packet, sizeof(WIRELESS_THERMOMETER_PacketTypeDef)) != NRF24_ERR_OK)
    {
        Error_Handler();
    }
    
    BOARD_RefreshWatchdog();
    
    
    bool completed = false;
    do
    {
        switch (NRF24_EndTransmit(&nrf24Handle))
        {
            case NRF24_ERR_TX_NO_ACK:
            case NRF24_ERR_TX_COMPLETE:
                completed = true;
                break;
            
            case NRF24_ERR_TX_NOT_COMPLETE:
                BOARD_RefreshWatchdog();
                continue;
            
            default:
                Error_Handler();
        }
    }
    while (!completed);
    
    BOARD_Standby(WIRELESS_THERMOMETER_NORMAL_WAIT_INTERVAL);
}
