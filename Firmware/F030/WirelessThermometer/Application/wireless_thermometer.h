#ifndef __WIRELESS_THERMOMETER_H
#define __WIRELESS_THERMOMETER_H


#include "stm32f0xx_hal.h"


#define WIRELESS_THERMOMETER_CONVERSION_TIME        ((uint16_t)1)
#define WIRELESS_THERMOMETER_NORMAL_WAIT_INTERVAL   ((uint16_t)59)
#define WIRELESS_THERMOMETER_ERROR_WAIT_INTERVAL    ((uint16_t)20)


__NO_RETURN void WIRELESS_THERMOMETER_Hook(void);


#endif /* __DEBUG_LOW_POWER_H */
