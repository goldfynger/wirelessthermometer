#ifndef __BOARD_H
#define __BOARD_H


#include <stdbool.h>
#include <stdint.h>
#include <time.h>
#include "stm32f0xx_hal.h"


typedef enum
{
    BOARD_RESET_UNKNOWN     = ((uint8_t)0x00),
    BOARD_RESET_LOW_POWER   = ((uint8_t)0x01),
    BOARD_RESET_WWDG        = ((uint8_t)0x02),
    BOARD_RESET_IWDG        = ((uint8_t)0x04),
    BOARD_RESET_SOFTWARE    = ((uint8_t)0x08),
    BOARD_RESET_POR         = ((uint8_t)0x10),
    BOARD_RESET_PIN         = ((uint8_t)0x20),
    BOARD_RESET_STANDBY     = ((uint8_t)0x40),
}
BOARD_ResetCauseTypeDef;


void                    BOARD_Init              (void);
void                    BOARD_GetDateTime       (struct tm *pRtcDateTime);
time_t                  BOARD_GetUpTime         (void);
void                    BOARD_ReadChipState     (int32_t *pChipTemperature, uint16_t *pChipVoltage);
void                    BOARD_Sleep             (uint16_t secondsTotal);
void                    BOARD_Stop              (uint16_t secondsTotal);
__NO_RETURN void        BOARD_Standby           (uint16_t secondsTotal);
void                    BOARD_RefreshWatchdog   (void);
BOARD_ResetCauseTypeDef BOARD_GetResetCause     (void);


#endif /* __BOARD_H */
