#ifndef __DEVICE_INFO_CONFIG_H
#define __DEVICE_INFO_CONFIG_H


#include <stdint.h>
#include "device_info.h"


/* Set device info here. */
#define DEVICE_INFO_MODEL       DEVICE_INFO_MODEL_WIRELESS_THERMOMETER_SENSOR_F030F4
#define DEVICE_INFO_REVISION    DEVICE_INFO_REVISION_PROTOTYPE
#define DEVICE_INFO_VERSION     ((DEVICE_INFO_VersionTypeDef)0)
#define DEVICE_INFO_SUBVERSION  ((DEVICE_INFO_SubversionTypeDef)1)


#endif /* __DEVICE_INFO_CONFIG_H */
