#include <stdbool.h>
#include <stdint.h>
#include "trace.h"
#include "board.h"
#include "debug_low_power.h"


__NO_RETURN void DEBUG_LOW_POWER_Hook(void)
{
    BOARD_Init();

    
    int32_t chipTemperature;
    uint16_t chipVoltage;
        
    BOARD_ReadChipState(&chipTemperature, &chipVoltage);
  
    TRACE_Format("%010u: Initialized: temperature %d m�C, voltage %d mV.\r\n", BOARD_GetUpTime(), chipTemperature, chipVoltage);
    
    
    while (true)
    {
        BOARD_RefreshWatchdog();
        
        TRACE_Format("%010u: Enter SLEEP mode.\r\n", BOARD_GetUpTime());
        
        BOARD_SuspendWatchdog(); /* Comment out this call for WWDG reset and switch to error handler. */
        
        BOARD_Sleep(59);
        
        BOARD_ResumeWatchdog();

        TRACE_Format("%010u: Wake up from SLEEP.\r\n", BOARD_GetUpTime());
        
        BOARD_RefreshWatchdog();
        
        TRACE_Format("%010u: Enter STOP mode.\r\n", BOARD_GetUpTime());
        
        //BOARD_SuspendWatchdog(); WWDG clock from MAIN sourse and disabled in STOP mode.
        
        BOARD_Stop(60);
        
        //BOARD_ResumeWatchdog();

        TRACE_Format("%010u: Wake up from STOP.\r\n", BOARD_GetUpTime());
        
        BOARD_RefreshWatchdog();
        
        BOARD_ReadChipState(&chipTemperature, &chipVoltage);
  
        TRACE_Format("%010u: Loop: temperature %d m�C, voltage %d mV.\r\n", BOARD_GetUpTime(), chipTemperature, chipVoltage);
        
        TRACE_Format("%010u: Enter STANDBY mode.\r\n", BOARD_GetUpTime());
        
        //BOARD_SuspendWatchdog(); WWDG clock from MAIN sourse and disabled in STANDBY mode.
        
        BOARD_Standby(61);
    }
}
