#ifndef __DEBUG_LOW_POWER_H
#define __DEBUG_LOW_POWER_H


#include "stm32f0xx_hal.h"


__NO_RETURN void DEBUG_LOW_POWER_Hook(void);


#endif /* __DEBUG_LOW_POWER_H */
