using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Goldfynger.UniversalDevice;
using Goldfynger.UniversalDevice.Connector;
using Goldfynger.UniversalDevice.Info;
using Goldfynger.UniversalDevice.Nrf24;

using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using WirelessThermometer.DataReceiver.Protos;

namespace WirelessThermometer.DataReceiver
{
    public class Receiver : BackgroundService
    {
        private readonly ILogger<Receiver> _logger;

        private readonly ConnectorSettings _connectorSettings;


        public Receiver(ILogger<Receiver> logger, ConnectorSettings connectorSettings)
        {
            _logger = logger;
            _connectorSettings = connectorSettings;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (true)
            {
                stoppingToken.ThrowIfCancellationRequested();

                try
                {
                    using var connector = CreateConnector();
                    _logger.LogInformation("Connector created: {connector}.", connector);

                    try
                    {
                        stoppingToken.ThrowIfCancellationRequested();

                        using var device = new DeviceFactory().Create(connector);

                        var deviceInfoInterface = device.CreateDeviceInfo();
                        var nrf24Interface = device.CreateNrf24Device();

                        try
                        {
                            stoppingToken.ThrowIfCancellationRequested();

                            await ReadDeviceInfo(deviceInfoInterface);

                            try
                            {
                                try
                                {
                                    stoppingToken.ThrowIfCancellationRequested();

                                    _logger.LogInformation("Try start listen.");
                                    await nrf24Interface.StartListenAsync();
                                    _logger.LogInformation("Success.");
                                }
                                catch (Nrf24Exception ex) when (ex.DeviceError == DeviceErrors.UNIVERSAL_DEVICE_ERR_COMMAND_INVALID_IN_CURRENT_CONTEXT)
                                {
                                    _logger.LogInformation("Listen already active.");
                                }
                                catch (Exception ex)
                                {
                                    _logger.LogError("Listen start exception: {exception}.", ex);
                                    throw;
                                }

                                try
                                {
                                    stoppingToken.ThrowIfCancellationRequested();

                                    var listener = nrf24Interface.GetListener();

                                    while (true)
                                    {
                                        stoppingToken.ThrowIfCancellationRequested();

                                        var rxData = await listener.ReadAsync(stoppingToken);

                                        stoppingToken.ThrowIfCancellationRequested();

                                        try
                                        {
                                            var sensorMessage = new TemperatureSensorMessage
                                            {
                                                Sensor = new TemperatureSensor
                                                {

                                                },
                                                Info = new TemperatureSensorInfo
                                                {

                                                },
                                            };


                                            //var sensorData = new TemperatureSensorMessage(rxData);

                                            //_logger.LogInformation("Rx data parsed {sensorData}.", sensorData);

                                            //NewMessage?.Invoke(this, new DataEventArgs<ITemperatureSensorMessage>(sensorData));
                                        }
                                        catch (Exception ex)
                                        {
                                            _logger.LogError("Rx data parse exception: {exception}.", ex);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    _logger.LogError("Listener exception: {exception}.", ex);
                                    throw;
                                }
                            }
                            catch
                            {
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError("Device info reading exception: {exception}.", ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError("Device creation exception: {exception}.", ex);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError("Connector creation exception: {exception}.", ex);
                }

                _logger.LogInformation("Delay for 5 seconds and try again.");
                await Task.Delay(5000, stoppingToken);
            }
        }

        private IDeviceConnector CreateConnector()
        {
            var connectorFactory = new DeviceConnectorFactory();

            var connectorType = connectorFactory.AvailableTypes.Where(type => type.Type == _connectorSettings.Type).FirstOrDefault();
            if (connectorType == null)
            {
                throw new InvalidOperationException($"Can not find connector with type \"{_connectorSettings.Type}\"." +
                    $" Available types is: { string.Join(", ", connectorFactory.AvailableTypes.Select(t => $"\"{t.Type}\"")) }");
            }

            var connectorIdentifier = connectorFactory.GetAvailableIdentifiers(connectorType).Where(id => id.Identifier == _connectorSettings.Identifier).FirstOrDefault();
            if (connectorIdentifier == null)
            {
                throw new InvalidOperationException($"Can not find connector with type \"{_connectorSettings.Type}\" and identifier \"{_connectorSettings.Identifier}\"." +
                    $" Available identifiers is: { string.Join(", ", connectorFactory.GetAvailableIdentifiers(connectorType).Select(i => $"\"{i.Identifier}\"")) }");
            }

            return connectorFactory.Create(connectorType, connectorIdentifier);
        }

        private async Task ReadDeviceInfo(IDeviceInfo deviceInfo)
        {
            var info = await deviceInfo.GetInfoAsync();
            var uid = await deviceInfo.GetUidAsync();

            _logger.LogInformation("Connected device: {info}.{uid}.", info, uid);
        }
    }
}
