﻿namespace WirelessThermometer.DataReceiver
{
    public sealed class ConnectorSettings
    {
        public string Type { get; set; }

        public string Identifier { get; set; }
    }
}
