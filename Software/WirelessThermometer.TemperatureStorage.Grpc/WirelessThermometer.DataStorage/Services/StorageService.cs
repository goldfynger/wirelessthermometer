﻿using System;
using System.Linq;
using System.Threading.Tasks;

using Google.Protobuf.WellKnownTypes;

using Grpc.Core;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace WirelessThermometer.DataStorage.Services
{
    public class StorageService : Protos.Storage.StorageBase
    {
        private readonly ILogger<StorageService> _logger;

        private readonly IServiceScopeFactory _scopeFactory;


        public StorageService(ILogger<StorageService> logger, IServiceScopeFactory scopeFactory)
        {
            _logger = logger;
            _scopeFactory = scopeFactory;

            using var scope = _scopeFactory.CreateScope();

            scope.ServiceProvider.GetRequiredService<Db.StorageContext>().Database.EnsureCreated();
        }

        
        public override Task<Empty> StoreTemperatureSensorMessage(Protos.TemperatureSensorMessage request, ServerCallContext context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();

                var dbContext = scope.ServiceProvider.GetRequiredService<Db.StorageContext>();

                var dbSensor = dbContext.TemperatureSensors.Where(s => s.Uid == request.Sensor.Uid).FirstOrDefault();

                if (dbSensor == null)
                {
                    dbSensor = new Db.TemperatureSensor
                    {
                        Uid = request.Sensor.Uid,
                    };
                    dbContext.TemperatureSensors.Add(dbSensor);
                }

                var dbMessage = new Db.TemperatureSensorMessage
                {
                    DateTime = request.DateTime.ToDateTime(),
                    UpTime = request.UpTime.ToTimeSpan(),
                    Temperature = request.Temperature,
                    ChipTemperature = request.ChipTemperature,
                    ChipVoltage = request.ChipVoltage,
                    ResetCause = request.ResetCause,
                };
                dbSensor.TemperatureSensorMessages.Add(dbMessage);

                var dbLastInfo = dbSensor.TemperatureSensorInfos.OrderByDescending(i => i.TemperatureSensorInfoId).LastOrDefault();
                if (dbLastInfo == null ||
                    dbLastInfo.Model != request.Info.Model || dbLastInfo.Revision != request.Info.Revision || dbLastInfo.Version != request.Info.Version || dbLastInfo.Subversion != request.Info.Subversion)
                {
                    var dbInfo = new Db.TemperatureSensorInfo
                    {
                        Model = request.Info.Model,
                        Revision = request.Info.Revision,
                        Version = request.Info.Version,
                        Subversion = request.Info.Subversion,
                    };

                    dbSensor.TemperatureSensorInfos.Add(dbInfo);
                    dbMessage.TemperatureSensorInfos.Add(dbInfo);                    
                }

                dbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _logger.LogError("Temperature sensor message storing exception: {exception}.", ex);
            }

            return Task.FromResult(new Empty());
        }
    }
}
