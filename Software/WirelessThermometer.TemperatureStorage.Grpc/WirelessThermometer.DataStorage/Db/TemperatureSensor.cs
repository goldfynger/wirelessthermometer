﻿using System.Collections.Generic;

namespace WirelessThermometer.DataStorage.Db
{
    /// <summary>
    /// Represents temperature sensor DB record.
    /// </summary>
    /// <remarks>Each sensor with unique identifier can have multiply info records (for example, if sensor softvare version is changed).</remarks>
    public sealed class TemperatureSensor
    {
        public int TemperatureSensorId { get; set; }

        public string Uid { get; set; }

        public List<TemperatureSensorInfo> TemperatureSensorInfos { get; set; } = new List<TemperatureSensorInfo>();
        public List<TemperatureSensorMessage> TemperatureSensorMessages { get; set; } = new List<TemperatureSensorMessage>();
    }
}
