﻿using System;
using System.Collections.Generic;

namespace WirelessThermometer.DataStorage.Db
{
    /// <summary>
    /// Represents temperature sensor message DB record.
    /// </summary>
    /// <remarks>Each sensor with unique identifier can have multiply info records (for example, if sensor softvare version is changed).</remarks>
    public sealed class TemperatureSensorMessage
    {
        public int TemperatureSensorMessageId { get; set; }

        public DateTime DateTime { get; set; }
        public TimeSpan UpTime { get; set; }
        public int Temperature { get; set; }
        public int ChipTemperature { get; set; }
        public uint ChipVoltage { get; set; }
        public string ResetCause { get; set; }

        public int TemperatureSensorId { get; set; }
        public TemperatureSensor TemperatureSensor { get; set; }

        public List<TemperatureSensorInfo> TemperatureSensorInfos { get; set; } = new List<TemperatureSensorInfo>();
    }
}
