﻿using Microsoft.EntityFrameworkCore;

namespace WirelessThermometer.DataStorage.Db
{
    public sealed class StorageContext : DbContext
    {
        public StorageContext()
        {
        }

        public StorageContext(DbContextOptions<StorageContext> options) : base(options)
        {
        }


        public DbSet<TemperatureSensor> TemperatureSensors { get; set; }

        public DbSet<TemperatureSensorMessage> TemperatureSensorMessages { get; set; }

        public DbSet<TemperatureSensorInfo> TemperatureSensorInfos { get; set; }
    }
}
