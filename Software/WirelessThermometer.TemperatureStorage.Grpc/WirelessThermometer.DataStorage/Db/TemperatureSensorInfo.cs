﻿namespace WirelessThermometer.DataStorage.Db
{
    /// <summary>
    /// Represents temperature sensor info DB record.
    /// </summary>
    /// <remarks>Each sensor with unique identifier can have multiply info records (for example, if sensor softvare version is changed).</remarks>
    public sealed class TemperatureSensorInfo
    {
        public int TemperatureSensorInfoId { get; set; }

        public string Model { get; set; }
        public string Revision { get; set; }
        public string Version { get; set; }
        public string Subversion { get; set; }

        public int TemperatureSensorId { get; set; }
        public TemperatureSensor TemperatureSensor { get; set; }

        public int TemperatureSensorMessageId { get; set; }
        public TemperatureSensorMessage TemperatureSensorMessage { get; set; }
    }
}
