using System;
using System.Collections.Generic;

using Goldfynger.Utils.Stm32.DeviceInfo;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Goldfynger.WirelessThermometer.TemperatureSensor.F030F4.MSTest
{
    [TestClass]
    public class MessageTest
    {
        [TestMethod]
        public void TryParseTest()
        {
            var nullMessageSource = (List<byte>)null;

            Assert.IsFalse(Message.TryParse(nullMessageSource, out Message nullMessage));
            Assert.AreEqual(null, nullMessage);


            var uid = new Uid(0x03020100, 0x07060504, 0x0B0A0908);            
            var information = new Information(
                new Model(Model.DeviceModels.DEVICE_INFO_MODEL_WIRELESS_THERMOMETER_SENSOR_F030F4),
                new Revision(Revision.DeviceRevisions.DEVICE_INFO_REVISION_EXAMPLE),
                new Utils.Stm32.DeviceInfo.Version(Utils.Stm32.DeviceInfo.Version.DeviceVersions.DEVICE_INFO_VERSION_EXAMPLE),
                new Subversion(Subversion.DeviceSubversions.DEVICE_INFO_SUBVERSION_EXAMPLE));
            var upTime = TimeSpan.FromSeconds(10);
            var temperature = -18063;
            var chipTemperature = 24938;
            var chipVoltage = (ushort)3393;
            var resetCause = Message.ResetCauses.BOARD_RESET_POR | Message.ResetCauses.BOARD_RESET_PIN;

            var messageSource = new List<byte>();
            messageSource.AddRange(information.Raw);
            messageSource.AddRange(uid.Raw);
            messageSource.AddRange(BitConverter.GetBytes(Convert.ToUInt32(upTime.TotalSeconds)));
            messageSource.AddRange(BitConverter.GetBytes(temperature));
            messageSource.AddRange(BitConverter.GetBytes(chipTemperature));
            messageSource.AddRange(BitConverter.GetBytes(chipVoltage));
            messageSource.Add((byte)resetCause);            

            Assert.IsFalse(Message.TryParse(messageSource, out Message invalidLengthMessage));
            Assert.AreEqual(null, invalidLengthMessage);


            messageSource.Add(0);

            Assert.IsTrue(Message.TryParse(messageSource, out Message message));
            Assert.AreEqual(information, message.Information);
            Assert.AreEqual(uid, message.Uid);
            Assert.AreEqual(upTime, message.UpTime);
            Assert.AreEqual(temperature, message.Temperature);
            Assert.AreEqual(chipTemperature, message.ChipTemperature);
            Assert.AreEqual(chipVoltage, message.ChipVoltage);
            Assert.AreEqual(resetCause, message.ResetCause);


            messageSource[0] = (byte)Model.DeviceModels.DEVICE_INFO_MODEL_EXAMPLE;

            Assert.IsFalse(Message.TryParse(messageSource, out Message invalidModelLengthMessage));
            Assert.AreEqual(null, invalidModelLengthMessage);
        }
    }
}
