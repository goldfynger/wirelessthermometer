﻿using System;
using System.Collections.Generic;
using System.Linq;

using Goldfynger.Utils.Extensions;
using Goldfynger.Utils.Stm32.DeviceInfo;

namespace Goldfynger.WirelessThermometer.TemperatureSensor.F030F4
{
    /// <summary>F030F4 temperature sensor message.</summary>
    public sealed record Message
    {
        private const byte __rxLength = 32;

        private const int __informationOffset = 0;
        private const int __uidOffset = 4;
        private const int __upTimeOffset = 16;
        private const int __temperatureOffset = 20;
        private const int __chipTemperatureOffset = 24;
        private const int __chipVoltageOffset = 28;
        private const int __resetCauseOffset = 30;


        private Message()
        {
        }


        /// <summary>Sensor uinque identifier.</summary>
        public Uid Uid { get; private init; }

        /// <summary>Sensor information.</summary>
        public Information Information { get; private init; }

        /// <summary>Sensor uptime.</summary>
        public TimeSpan UpTime { get; private init; }

        /// <summary>Sensor temperature.</summary>
        public int Temperature { get; private init; }

        /// <summary>Chip temperature.</summary>
        public int ChipTemperature { get; private init; }

        /// <summary>Chip voltage</summary>
        public ushort ChipVoltage { get; private init; }

        /// <summary>Sensor reset cause.</summary>
        public ResetCauses ResetCause { get; private init; }


        /// <summary>Tries to parse <see cref="Message"/> using <see cref="List{T}"/> of <see cref="byte"/> as a source.</summary>
        /// <param name="data">Input <see cref="List{T}"/> of <see cref="byte"/>.</param>
        /// <param name="message">Result <see cref="Message"/> or <see langword="null"/> if parsing failed.</param>
        /// <returns><see langword="true"/> if <see cref="Message"/> was parsed successfully; otherwise, <see langword="false"/>.</returns>
        public static bool TryParse(IList<byte> data, out Message message)
        {
            message = null;

            if (data == null)
            {
                return false;
            }

            if (data.Count != __rxLength)
            {
                return false;
            }

            var information = new Information(data.Copy(__informationOffset, Information.Size));

            if (information.Model.Value != (byte)Model.DeviceModels.DEVICE_INFO_MODEL_WIRELESS_THERMOMETER_SENSOR_F030F4)
            {
                return false;
            }

            var source = data.ToArray();

            message = new Message
            {
                Uid = new Uid(data.Copy(__uidOffset, Uid.Size)),
                Information = information,
                UpTime = TimeSpan.FromSeconds(BitConverter.ToUInt32(source, __upTimeOffset)),
                Temperature = BitConverter.ToInt32(source, __temperatureOffset),
                ChipTemperature = BitConverter.ToInt32(source, __chipTemperatureOffset),
                ChipVoltage = BitConverter.ToUInt16(source, __chipVoltageOffset),
                ResetCause = (ResetCauses)source[__resetCauseOffset],
            };

            return true;
        }

        /// <summary>Sensor reset causes.</summary>
        [Flags]
        public enum ResetCauses : byte
        {
            /// <summary>Unknown reset cause.</summary>
            BOARD_RESET_UNKNOWN = 0x00,
            /// <summary>Low power reset.</summary>
            BOARD_RESET_LOW_POWER = 0x01,
            /// <summary>Window watchdog reset.</summary>
            BOARD_RESET_WWDG = 0x02,
            /// <summary>Independent watchdog reset.</summary>
            BOARD_RESET_IWDG = 0x04,
            /// <summary>Software reset.</summary>
            BOARD_RESET_SOFTWARE = 0x08,
            /// <summary>POR reset.</summary>
            BOARD_RESET_POR = 0x10,
            /// <summary>PIN reset.</summary>
            BOARD_RESET_PIN = 0x20,
            /// <summary>STANDBY mode reset.</summary>
            BOARD_RESET_STANDBY = 0x40,
        }
    }
}
