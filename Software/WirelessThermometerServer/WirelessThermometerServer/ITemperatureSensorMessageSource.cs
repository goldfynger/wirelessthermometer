﻿using System;

using Goldfynger.Utils.DataContainers;

namespace WirelessThermometerServer
{
    public interface ITemperatureSensorMessageSource
    {
        event EventHandler<DataEventArgs<ITemperatureSensorMessage>> NewMessage;
    }
}
