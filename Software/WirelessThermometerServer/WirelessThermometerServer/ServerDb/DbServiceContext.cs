﻿
using Microsoft.EntityFrameworkCore;

namespace WirelessThermometerServer.ServerDb
{
    public sealed class DbServiceContext : DbContext
    {
        public DbServiceContext()
        {
        }

        public DbServiceContext(DbContextOptions<DbServiceContext> options) : base(options)
        {
        }


        public DbSet<TemperatureSensor> TemperatureSensors { get; set; }

        public DbSet<TemperatureSensorMessage> TemperatureSensorMessages { get; set; }

        public DbSet<TemperatureSensorInfo> TemperatureSensorInfos { get; set; }
    }
}
