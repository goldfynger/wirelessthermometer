using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Goldfynger.Utils.DataContainers;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace WirelessThermometerServer.ServerDb
{
    public sealed class DbService : BackgroundService
    {
        private readonly ILogger<DbService> _logger;

        private readonly IServiceScopeFactory _scopeFactory;

        private readonly ITemperatureSensorMessageSource _temperatureSensorDataSource;


        public DbService(ILogger<DbService> logger, IServiceScopeFactory scopeFactory, ITemperatureSensorMessageSource temperatureSensorDataSource)
        {
            _logger = logger;
            _scopeFactory = scopeFactory;
            _temperatureSensorDataSource = temperatureSensorDataSource;
        }


        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            void handler (object sender, DataEventArgs<ITemperatureSensorMessage> e)
            {
                try
                {
                    using var scope = _scopeFactory.CreateScope();

                    var dbContext = scope.ServiceProvider.GetRequiredService<DbServiceContext>();

                    var sensorUid = e.Data.Uid;
                    var sensor = dbContext.TemperatureSensors.Where(s => s.Uid == sensorUid).FirstOrDefault();

                    if (sensor == null)
                    {
                        sensor = new TemperatureSensor
                        {
                            Uid = sensorUid,
                        };
                        dbContext.TemperatureSensors.Add(sensor);
                        dbContext.SaveChanges();
                    }

                    var message = new TemperatureSensorMessage
                    {
                        UpTime = e.Data.UpTime,
                        Temperature = e.Data.Temperature,
                        ChipTemperature = e.Data.ChipTemperature,
                        ChipVoltage = e.Data.ChipVoltage,
                        ResetCause = e.Data.ResetCause,
                        DateTime = e.Data.UtcDateTime,
                    };
                    sensor.TemperatureSensorMessages.Add(message);
                    dbContext.SaveChanges();

                    var lastInfo = dbContext.TemperatureSensorInfos.OrderByDescending(i => i.TemperatureSensorInfoId).LastOrDefault(i => i.TemperatureSensorId == sensor.TemperatureSensorId);
                    if (lastInfo == null || lastInfo.Model != e.Data.Model || lastInfo.Revision != e.Data.Revision || lastInfo.Version != e.Data.Version || lastInfo.Subversion != e.Data.Subversion)
                    {
                        var info = new TemperatureSensorInfo
                        {
                            Model = e.Data.Model,
                            Revision = e.Data.Revision,
                            Version = e.Data.Version,
                            Subversion = e.Data.Subversion,
                        };

                        sensor.TemperatureSensorInfos.Add(info);
                        message.TemperatureSensorInfos.Add(info);
                        dbContext.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError("New message handling exception: {exception}.", ex);
                }
            }

            try
            {
                _temperatureSensorDataSource.NewMessage += handler;

                while (!stoppingToken.IsCancellationRequested)
                {
                    await Task.Delay(1000);
                }
            }
            finally
            {
                _temperatureSensorDataSource.NewMessage -= handler;
            }
        }
    }
}
