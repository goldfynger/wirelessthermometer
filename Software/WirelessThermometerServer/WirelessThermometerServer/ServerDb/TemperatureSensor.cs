﻿using System.Collections.Generic;

namespace WirelessThermometerServer.ServerDb
{
    public sealed class TemperatureSensor
    {
        public int TemperatureSensorId { get; set; }

        public string Uid { get; set; }

        public List<TemperatureSensorInfo> TemperatureSensorInfos { get; set; } = new List<TemperatureSensorInfo>();
        public List<TemperatureSensorMessage> TemperatureSensorMessages { get; set; } = new List<TemperatureSensorMessage>();
    }
}
