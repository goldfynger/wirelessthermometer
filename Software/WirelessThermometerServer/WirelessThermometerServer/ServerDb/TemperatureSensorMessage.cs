﻿using System;
using System.Collections.Generic;

namespace WirelessThermometerServer.ServerDb
{

    public sealed class TemperatureSensorMessage
    {
        public int TemperatureSensorMessageId { get; set; }

        public TimeSpan UpTime { get; set; }
        public int Temperature { get; set; }
        public int ChipTemperature { get; set; }
        public ushort ChipVoltage { get; set; }
        public string ResetCause { get; set; }

        public DateTime DateTime { get; set; }

        public int TemperatureSensorId { get; set; }
        public TemperatureSensor TemperatureSensor { get; set; }

        public List<TemperatureSensorInfo> TemperatureSensorInfos { get; set; } = new List<TemperatureSensorInfo>();
    }
}
