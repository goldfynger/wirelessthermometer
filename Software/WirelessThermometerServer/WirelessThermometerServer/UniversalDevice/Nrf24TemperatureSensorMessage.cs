﻿using System;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;

using Goldfynger.UniversalDevice.Info;
using Goldfynger.UniversalDevice.Nrf24;

namespace WirelessThermometerServer.UniversalDevice
{
    public sealed class Nrf24TemperatureSensorMessage : ITemperatureSensorMessage
    {
        private const int __deviceInfoOffset = 0;
        private const int __deviceUidOffset = 4;
        private const int __upTimeOffset = 16;
        private const int __temperatureOffset = 20;
        private const int __chipTemperatureOffset = 24;
        private const int __chipVoltageOffset = 28;
        private const int __resetCauseOffset = 30;


        public Nrf24TemperatureSensorMessage(Nrf24RxData rxData)
        {
            var sourceArray = rxData.Data.ToArray();

            var deviceInfo = new DeviceInformation(sourceArray, __deviceInfoOffset);
            var deviceUid = new DeviceUid(sourceArray, __deviceUidOffset);
            var upTime = BitConverter.ToUInt32(sourceArray, __upTimeOffset);
            var temperature = BitConverter.ToInt32(sourceArray, __temperatureOffset);
            var chipTemperature = BitConverter.ToInt32(sourceArray, __chipTemperatureOffset);
            var chipVoltage = BitConverter.ToUInt16(sourceArray, __chipVoltageOffset);
            var resetCause = (ResetCauses)sourceArray[__resetCauseOffset];

            Uid = deviceUid.ToString();

            Model = deviceInfo.Model.ToString();
            Revision = deviceInfo.Revision.ToString();
            Version = deviceInfo.Version.ToString();
            Subversion = deviceInfo.Subversion.ToString();

            UpTime = TimeSpan.FromSeconds(upTime);
            Temperature = temperature;
            ChipTemperature = chipTemperature;
            ChipVoltage = chipVoltage;
            ResetCause = resetCause.ToString();

            UtcDateTime = DateTime.UtcNow;

            Hardware = $"{deviceInfo.Model}.Rev_{deviceInfo.Revision}";
            Firmware = $"Ver_{deviceInfo.Version}.{deviceInfo.Subversion}";
        }


        public string Uid { get; }

        [JsonIgnore]
        public string Model { get; }
        [JsonIgnore]
        public string Revision { get; }
        [JsonIgnore]
        public string Version { get; }
        [JsonIgnore]
        public string Subversion { get; }

        [JsonConverter(typeof(UpTimeJsonConverter))]
        public TimeSpan UpTime { get; }
        [JsonConverter(typeof(TemperatureJsonConverter))]
        public int Temperature { get; }
        [JsonConverter(typeof(TemperatureJsonConverter))]
        public int ChipTemperature { get; }
        [JsonConverter(typeof(VoltageJsonConverter))]
        public ushort ChipVoltage { get; }
        public string ResetCause { get; }

        public DateTime UtcDateTime { get; }


        public string Hardware { get; }
        public string Firmware { get; }


        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }


        [Flags]
        public enum ResetCauses : byte
        {
            BOARD_RESET_UNKNOWN = 0x00,
            BOARD_RESET_LOW_POWER = 0x01,
            BOARD_RESET_WWDG = 0x02,
            BOARD_RESET_IWDG = 0x04,
            BOARD_RESET_SOFTWARE = 0x08,
            BOARD_RESET_POR = 0x10,
            BOARD_RESET_PIN = 0x20,
            BOARD_RESET_STANDBY = 0x40,
        }


        private sealed class TemperatureJsonConverter : JsonConverter<int>
        {
            public override int Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
            {
                throw new NotSupportedException();
            }

            public override void Write(Utf8JsonWriter writer, int value, JsonSerializerOptions options)
            {
                writer.WriteStringValue($"{value / 1000}.{value % 1000}");
            }
        }

        private sealed class VoltageJsonConverter : JsonConverter<ushort>
        {
            public override ushort Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
            {
                throw new NotSupportedException();
            }

            public override void Write(Utf8JsonWriter writer, ushort value, JsonSerializerOptions options)
            {
                writer.WriteStringValue($"{value / 1000}.{value % 1000}");
            }
        }

        private sealed class UpTimeJsonConverter : JsonConverter<TimeSpan>
        {
            public override TimeSpan Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
            {
                throw new NotSupportedException();
            }

            public override void Write(Utf8JsonWriter writer, TimeSpan value, JsonSerializerOptions options)
            {
                writer.WriteStringValue(value.ToString());
            }
        }
    }
}
