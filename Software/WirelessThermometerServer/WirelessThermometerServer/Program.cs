using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using WirelessThermometerServer.ServerDb;
using WirelessThermometerServer.UniversalDevice;

namespace WirelessThermometerServer
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseWindowsService()
                .UseSystemd()
                //.ConfigureLogging(loggerFactory => loggerFactory.AddEventLog())
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddDbContext<DbServiceContext>(options => options.UseSqlite(hostContext.Configuration.GetConnectionString("SQLiteConnection")));

                    services.AddSingleton(hostContext.Configuration.GetSection("ConnectorSettings").Get<ConnectorSettings>());

                    services.AddHostedService<ITemperatureSensorMessageSource, Nrf24TemperatureSensorMessageSource>();

                    services.AddHostedService<DbService>();
                });
    }
}
