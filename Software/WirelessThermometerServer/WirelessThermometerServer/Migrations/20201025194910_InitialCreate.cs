﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WirelessThermometerServer.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TemperatureSensors",
                columns: table => new
                {
                    TemperatureSensorId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Uid = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TemperatureSensors", x => x.TemperatureSensorId);
                });

            migrationBuilder.CreateTable(
                name: "TemperatureSensorMessages",
                columns: table => new
                {
                    TemperatureSensorMessageId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UpTime = table.Column<TimeSpan>(nullable: false),
                    Temperature = table.Column<int>(nullable: false),
                    ChipTemperature = table.Column<int>(nullable: false),
                    ChipVoltage = table.Column<ushort>(nullable: false),
                    ResetCause = table.Column<string>(nullable: true),
                    DateTime = table.Column<DateTime>(nullable: false),
                    TemperatureSensorId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TemperatureSensorMessages", x => x.TemperatureSensorMessageId);
                    table.ForeignKey(
                        name: "FK_TemperatureSensorMessages_TemperatureSensors_TemperatureSensorId",
                        column: x => x.TemperatureSensorId,
                        principalTable: "TemperatureSensors",
                        principalColumn: "TemperatureSensorId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TemperatureSensorInfos",
                columns: table => new
                {
                    TemperatureSensorInfoId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Model = table.Column<string>(nullable: true),
                    Revision = table.Column<string>(nullable: true),
                    Version = table.Column<string>(nullable: true),
                    Subversion = table.Column<string>(nullable: true),
                    TemperatureSensorId = table.Column<int>(nullable: false),
                    TemperatureSensorMessageId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TemperatureSensorInfos", x => x.TemperatureSensorInfoId);
                    table.ForeignKey(
                        name: "FK_TemperatureSensorInfos_TemperatureSensors_TemperatureSensorId",
                        column: x => x.TemperatureSensorId,
                        principalTable: "TemperatureSensors",
                        principalColumn: "TemperatureSensorId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TemperatureSensorInfos_TemperatureSensorMessages_TemperatureSensorMessageId",
                        column: x => x.TemperatureSensorMessageId,
                        principalTable: "TemperatureSensorMessages",
                        principalColumn: "TemperatureSensorMessageId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TemperatureSensorInfos_TemperatureSensorId",
                table: "TemperatureSensorInfos",
                column: "TemperatureSensorId");

            migrationBuilder.CreateIndex(
                name: "IX_TemperatureSensorInfos_TemperatureSensorMessageId",
                table: "TemperatureSensorInfos",
                column: "TemperatureSensorMessageId");

            migrationBuilder.CreateIndex(
                name: "IX_TemperatureSensorMessages_TemperatureSensorId",
                table: "TemperatureSensorMessages",
                column: "TemperatureSensorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TemperatureSensorInfos");

            migrationBuilder.DropTable(
                name: "TemperatureSensorMessages");

            migrationBuilder.DropTable(
                name: "TemperatureSensors");
        }
    }
}
