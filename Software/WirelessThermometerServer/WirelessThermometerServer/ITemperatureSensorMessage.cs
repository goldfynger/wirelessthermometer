﻿using System;

namespace WirelessThermometerServer
{
    public interface ITemperatureSensorMessage
    {
        string Uid { get; }

        string Model { get; }
        string Revision { get; }
        string Version { get; }
        string Subversion { get; }

        TimeSpan UpTime { get; }
        int Temperature { get; }
        int ChipTemperature { get; }
        ushort ChipVoltage { get; }
        string ResetCause { get; }

        DateTime UtcDateTime { get; }
    }
}
