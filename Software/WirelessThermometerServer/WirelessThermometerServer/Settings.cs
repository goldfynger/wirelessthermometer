﻿using System;
using System.IO;
using System.Linq;
using System.Text.Json;

namespace WirelessThermometerServer
{
    public sealed class SettingsManager : IDisposable
    {
        private readonly JsonDocument _jsonDocument;


        public SettingsManager(string settingsPath)
        {
            _jsonDocument = JsonDocument.Parse(File.ReadAllText(settingsPath));
        }

        
        public TSettings GetSettings<TSettings>()
        {
            var jsonProperty = _jsonDocument.RootElement.EnumerateObject().Where(p => p.Name == typeof(TSettings).Name).First();

            return JsonSerializer.Deserialize<TSettings>(jsonProperty.Value.GetRawText());
        }


        private bool _disposed;

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _jsonDocument?.Dispose();
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                _disposed = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~WirelessThermometerServerSettings()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
